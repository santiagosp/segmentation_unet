# README #

This repository is used as an instance of training for segmentation models with U-NET.

### What is this repository for? ###

* U-NET segmentation train

### How do I get set up? ###

* You need to make sure you work with CUDA-10.0 and corresponding CUDnn and NVIDIA drivers, python3.7 or older.
* If not, follow the steps marked in environment_preparation.txt.
* The training is done on a virtual environment (venv_segmentation).
* Having the environment activated, go to segmentation_folder path.
* Review the configuration file is correct "./config/config.ini"
* In the terminal run "bash train_segmentation.sh".
* Wait till the model trains completely.
* Weights will be in the same path you ran the bash file.

### Who do I talk to? ###

* For any doubts, check with santiago@gestalabs.com