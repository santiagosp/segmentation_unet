FROM nvidia/cuda:11.3.0-cudnn8-devel-ubuntu20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && apt-get install -y \ 
                         python3-opencv libopencv-dev \
                         python3-distutils python3-apt python3-pip \
                         ffmpeg git

RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.8 1 && \
    update-alternatives  --set python /usr/bin/python3.8

WORKDIR /home


COPY ./segmentation_training/config/ /home/segmentation_training/config

WORKDIR /home/segmentation_training

RUN pip install -r config/requirements.txt

CMD ["bash","train_segmentation.sh"]
