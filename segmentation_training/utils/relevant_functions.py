from dataloader import denormalize
import segmentation_models as sm
import tensorflow as tf
import matplotlib.pyplot as plt
import configparser
import numpy as np
import subprocess
import shutil
import json
import io
import os

config = configparser.RawConfigParser()
config.read('config/config.ini')
training_config = dict(config.items('U-NET'))

CLASSES = json.loads(training_config['list_classes'])
ROOT_PATH = training_config['root_path']

def copy_files(paths_list, ROOT_PATH,original_folder, target_folder):
  """ Copy files from one folder to another for splitting purposes.
  Args:
      paths_list (list): A list of image filenames.
      original_folder (str): The name of the original folder where files are
      target_folder (str): The name of the folder where the images will be copied. 
  """
  try:
    os.makedirs(target_folder)
  except Exception as e:
    subprocess.call('rm -rf {}'.format(target_folder), cwd=ROOT_PATH, shell=True)
    # remove_files(os.path.join(ROOT_PATH, SEGMENTATION_FOLDER, target_folder))
    os.makedirs(target_folder)
  for file in paths_list:  
    original_path = os.path.join(original_folder, file)
    target_path = os.path.join(target_folder, file)
    shutil.copyfile(original_path, target_path)
  print("Copy of {} done!".format(target_folder))

def create_model(CLASSES,BACKBONE,WIDTH = 1024,HEIGHT = 1024):
  """ Creates a Unet model depending on the classes.
  Args:
      CLASSES (list): A list of the classes to be trained.
      BACKBONE (str): The name of model architecture to be used. 
  Returns:
      n_classses (int): Number of classes of the model.
      activation (str): Activation function used for the segmentation
      model: Model created with the required properties.
  """
  # define network parameters
  n_classes = 1 if len(CLASSES) == 1 else (len(CLASSES) + 1)  # case for binary and multiclass segmentation
  print(n_classes)
  activation = 'sigmoid' if n_classes == 1 else 'softmax'
  #create model
  model = sm.Unet(BACKBONE, classes=n_classes, activation=activation, input_shape=(WIDTH,HEIGHT,3))
  return n_classes,activation,model


def weights_name(n_classes):
  """ Define the name in which the h5 file of the weights will be.
  Args:
      n_classes (int): the number of classes the model is working on.
  Returns:
      weights_filename (str): Name of the file for the weights. 
  """
  if n_classes == 1:
    weights_filename = 'best_model_binary_512.h5'
  else:
    weights_filename = 'best_model_512.h5'
  return weights_filename


# helper function for data visualization
def visualize(**images):
  """Plot images in one row.
  Args:
     images (array,variable): Variable input of images to be displayed.
  """
  n = len(images)
  plt.figure(figsize=(16, 5))
  for i, (name, image) in enumerate(images.items()):
      plt.subplot(1, n, i + 1)
      plt.xticks([])
      plt.yticks([])
      plt.title(' '.join(name.split('_')).title())
      plt.imshow(image)
  plt.show()

# Tensorboard plotting functions
def image_grid(val_dataset,model,pred = False):
    figure = plt.figure(figsize=(20, 20))
    for img_idx in range(10):
        img, gt_mask = val_dataset[img_idx] # get some sample
        if pred:
            img = np.expand_dims(img, axis=0)
            pr_mask = model.predict(img)
        mask_dic = {'image':denormalize(img.squeeze())}
        for i in range(len(CLASSES)):
            mask_dic[CLASSES[i]+"_mask"] = gt_mask[...,i].squeeze()
            if pred:
                mask_dic["pr_"+CLASSES[i]+"_mask"] = pr_mask[...,i].squeeze()
        n = len(mask_dic)
        for i, (name, image) in enumerate(mask_dic.items()):
            plt.subplot(10, n, n*img_idx + i + 1)
            plt.xticks([])
            plt.yticks([])
            plt.title(' '.join(name.split('_')).title())
            plt.imshow(image)
    # plt.show()
    return figure

def plot_to_image(figure):    
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    plt.close(figure)
    buf.seek(0)

    digit = tf.image.decode_png(buf.getvalue(), channels=4)
    digit = tf.expand_dims(digit, 0)
    return digit


def log_masks_epochs(epoch,logs):
    file_writer = tf.summary.create_file_writer(os.path.join(ROOT_PATH, 'tb_logs'))
    figure = image_grid(pred = True)
    with file_writer.as_default():
        tf.summary.image("Val data examples", plot_to_image(figure), step=epoch)