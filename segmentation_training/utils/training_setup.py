import warnings
warnings.filterwarnings("ignore")

from dataloader import get_training_augmentation, get_validation_augmentation, get_preprocessing
from relevant_functions import create_model, weights_name, log_masks_epochs
from tensorflow.keras.optimizers import Adam
import segmentation_models as sm
from tensorflow import keras
import mlflow.keras
import configparser
import dataloader
import tensorflow
import subprocess
import mlflow
import json
import os

print("Keras: " + keras.__version__)
print("Tensorflow: " + tensorflow.__version__)
sm.set_framework('tf.keras')



config = configparser.RawConfigParser()
config.read('config/config.ini')
training_config = dict(config.items('U-NET'))

LIST_CLASSES = json.loads(training_config['list_classes'])
ROOT_PATH = training_config['root_path']
VAL_PROP = float(training_config['val_prop'])
TEST_PROP = float(training_config['test_prop'])
BACKBONE = training_config['backbone']
BATCH_SIZE = int(training_config['batch_size'])
WIDTH = int(training_config['width'])
HEIGHT = int(training_config['height'])
LR = float(training_config['learning_rate'])
EPOCHS = int(training_config['epochs'])
LABELS_FORMAT = training_config['labels_format']

SEGMENTATION_PATH = os.path.join(ROOT_PATH, "segmentation")

# Run masks_generator
import masks_generator
# subprocess.call('python utils/masks_generator.py', shell=True)

# Run dataset generator
import dataset_generator
# subprocess.call('python utils/dataset_generator.py', shell=True)

#Create Model
preprocess_input = sm.get_preprocessing(BACKBONE)
optim = Adam(LR)
n_classes,activation,model = create_model(LIST_CLASSES, BACKBONE,WIDTH,HEIGHT)
total_loss = sm.losses.categorical_focal_dice_loss 
metrics = [sm.metrics.IOUScore(threshold=0.5), sm.metrics.FScore(threshold=0.5)]
model.compile(optim, total_loss, metrics)

# Dataset for train images
train_dataset = dataloader.Dataset(
    dataset_generator.x_train_dir, 
    dataset_generator.y_train_dir, 
    classes=LIST_CLASSES, 
    augmentation=get_training_augmentation(HEIGHT,WIDTH),
    preprocessing=get_preprocessing(preprocess_input),
)

# Dataset for validation images
val_dataset = dataloader.Dataset(
    dataset_generator.x_val_dir, 
    dataset_generator.y_val_dir, 
    classes=LIST_CLASSES, 
    augmentation=get_validation_augmentation(HEIGHT,WIDTH),
    preprocessing=get_preprocessing(preprocess_input),
)

train_dataloader = dataloader.Dataloder(train_dataset, batch_size=BATCH_SIZE, shuffle=True)
val_dataloader = dataloader.Dataloder(val_dataset, batch_size=1, shuffle=False)

# check shapes for errors
assert train_dataloader[0][0].shape == (BATCH_SIZE, WIDTH, HEIGHT, 3)
assert train_dataloader[0][1].shape == (BATCH_SIZE, WIDTH, HEIGHT, n_classes)

# define callbacks for learning rate scheduling and best checkpoints saving
weights_filename = weights_name(n_classes)
callbacks = [
    keras.callbacks.ModelCheckpoint(os.path.join(ROOT_PATH,weights_filename), save_weights_only=True, save_best_only=True, mode='min'),
    keras.callbacks.ReduceLROnPlateau(),
    keras.callbacks.TensorBoard(log_dir=os.path.join(ROOT_PATH, 'tb_logs')),
    keras.callbacks.LambdaCallback(on_epoch_end=log_masks_epochs)
]

subprocess.Popen(["tensorboard","--logdir","{}/tb_logs/".format(ROOT_PATH),"--bind_all"])

# Train model
mlflow.keras.autolog()
mlflow.set_tracking_uri("file:////{}/mlruns".format(ROOT_PATH))
history = model.fit(
    train_dataloader, 
    steps_per_epoch=len(train_dataloader), 
    epochs=EPOCHS, 
    callbacks=callbacks, 
    validation_data=val_dataloader,
    validation_steps=len(val_dataloader),
)