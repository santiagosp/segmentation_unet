from natsort import natsorted
import albumentations as A
import numpy as np
import keras
import cv2
import os
  
# helper function for data visualization    
def denormalize(x):
  """Scale image to range 0.1 for correct plot
  Args:
      x (numpy array): Numpy array of an image.
  Returns:
      x (numoy array): Denormalized numpy array of an image.
  """
  x_max = np.percentile(x, 98)
  x_min = np.percentile(x, 2)    
  x = (x - x_min) / (x_max - x_min)
  x = x.clip(0, 1)
  return x
    

# classes for data loading and preprocessing
class Dataset:
    """CamVid Dataset. Read images, apply augmentation and preprocessing transformations.
    
    Args:
        images_dir (str): path to images folder
        masks_dir (str): path to segmentation masks folder
        class_values (list): values of classes to extract from segmentation mask
        augmentation (albumentations.Compose): data transfromation pipeline 
            (e.g. flip, scale, etc.)
        preprocessing (albumentations.Compose): data preprocessing 
            (e.g. normalization, shape manipulation, etc.)
    
    """
    
    def __init__(
            self, 
            images_dir, 
            masks_dir, 
            classes=None,
            augmentation=None, 
            preprocessing=None,
            class_type='crops',
            width = 1024,
            height = 1024
    ):
        self.CLASSES = {"crops" : ["background"] + classes}
        self.ids = natsorted(os.listdir(images_dir))
        self.m_ids = natsorted(os.listdir(masks_dir))
        self.images_fps = [os.path.join(images_dir, image_id) for image_id in self.ids]
        self.masks_fps = [os.path.join(masks_dir, mask_id) for mask_id in self.m_ids]
        self.WIDTH = width
        self.HEIGHT = height
        
        # convert str names to class values on masks
        self.class_values = [self.CLASSES[class_type].index(cls.lower()) for cls in classes]
        
        self.augmentation = augmentation
        self.preprocessing = preprocessing
    
    def __getitem__(self, i):
        # read data
        #print(self.images_fps[i])
        image = cv2.imread(self.images_fps[i])
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        gray_img = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        gray_img_3c = np.zeros_like(image)
        for channel in range(3):
          gray_img_3c[:,:,channel] = gray_img.copy()
        image = gray_img_3c.copy()
        image = cv2.resize(image, (self.HEIGHT, self.WIDTH))
        mask = cv2.imread(self.masks_fps[i], 0)
        mask = cv2.resize(mask, (self.HEIGHT, self.WIDTH), interpolation = cv2.INTER_NEAREST)
        
        # extract certain classes from mask (e.g. cars)
        masks = [(mask == v) for v in self.class_values]
        mask = np.stack(masks, axis=-1).astype('float')
        
        # add background if mask is not binary
        if mask.shape[-1] != 1:
            background = 1 - mask.sum(axis=-1, keepdims=True)
            mask = np.concatenate((mask, background), axis=-1)
        
        # apply augmentations
        if self.augmentation:
            sample = self.augmentation(image=image, mask=mask)
            image, mask = sample['image'], sample['mask']
        
        # apply preprocessing
        if self.preprocessing:
            sample = self.preprocessing(image=image, mask=mask)
            image, mask = sample['image'], sample['mask']
            
        return image, mask
        
    def __len__(self):
        return len(self.ids)
    
    
class Dataloder(keras.utils.Sequence):
    """Load data from dataset and form batches
    
    Args:
        dataset: instance of Dataset class for image loading and preprocessing.
        batch_size: Integet number of images in batch.
        shuffle: Boolean, if `True` shuffle image indexes each epoch.
    """
    
    def __init__(self, dataset, batch_size=1, shuffle=False):
        self.dataset = dataset
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.indexes = np.arange(len(dataset))

        self.on_epoch_end()

    def __getitem__(self, i):
        
        # collect batch data
        start = i * self.batch_size
        stop = (i + 1) * self.batch_size
        data = []
        for j in range(start, stop):
            data.append(self.dataset[j])
        
        # transpose list of lists
        batch = [np.stack(samples, axis=0) for samples in zip(*data)]
        
        return batch
    
    def __len__(self):
        """Denotes the number of batches per epoch"""
        return len(self.indexes) // self.batch_size
    
    def on_epoch_end(self):
        """Callback function to shuffle indexes each epoch"""
        if self.shuffle:
            self.indexes = np.random.permutation(self.indexes)   

def round_clip_0_1(x, **kwargs):
  """ Round value of the value """
  return x.round().clip(0, 1)

# define heavy augmentations
def get_training_augmentation(HEIGHT = 1024, WIDTH = 1024):
  """ Augmenations Transformations to image """
  train_transform = [

      A.HorizontalFlip(p=0.5),

      A.VerticalFlip(p=0.5),

      #A.ShiftScaleRotate(scale_limit=0.5, rotate_limit=0, shift_limit=0.1, p=1, border_mode=0),

      A.PadIfNeeded(min_height=HEIGHT, min_width=WIDTH, always_apply=True, border_mode=0),

      A.GaussNoise (p=0.2),
      A.Perspective (p=0.5),

      A.OneOf(
          [
              A.CLAHE(p=1),
              A.RandomBrightness(p=1),
              A.RandomGamma(p=1),
          ],
          p=0.9,
      ),

      A.OneOf(
          [
              A.Sharpen (p=1),
              A.Blur(blur_limit=3, p=1),
              A.MotionBlur(blur_limit=3, p=1),
          ],
          p=0.9,
      ),

      A.OneOf(
          [
              A.RandomContrast(p=1),
              A.HueSaturationValue(p=1),
          ],
          p=0.9,
      ),
      A.Lambda(mask=round_clip_0_1)
  ]
  return A.Compose(train_transform)


def get_validation_augmentation(HEIGHT = 1024, WIDTH = 1024):
    """Add paddings to make image shape divisible by 32"""
    test_transform = [
        A.PadIfNeeded(HEIGHT, WIDTH)
    ]
    return A.Compose(test_transform)

def get_preprocessing(preprocessing_fn):
    """Construct preprocessing transform
    
    Args:
        preprocessing_fn (callbale): data normalization function 
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose
    
    """
    
    _transform = [
        A.Lambda(image=preprocessing_fn),
    ]
    return A.Compose(_transform)