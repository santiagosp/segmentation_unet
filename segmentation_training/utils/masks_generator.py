from pycocotools.coco import COCO
from shapely.geometry import Polygon
from binascii import Error
import configparser
import subprocess
import numpy as np
import json
import cv2
import os



config = configparser.RawConfigParser()
config.read('config/config.ini')

training_config = dict(config.items('U-NET'))
ROOT_PATH = training_config['root_path']
LABELS_FORMAT = training_config['labels_format']
MASKS_PATH = os.path.join(ROOT_PATH,"data/masks")
JSON_PATH = os.path.join(ROOT_PATH,"data/json")
RAW_IMAGES_PATH = os.path.join(ROOT_PATH,"data/images")
RAW_IMAGES_EXTENSION = training_config['images_extension']
MASKS_EXTENSION = ".png"
errors = []

try:
  os.makedirs(MASKS_PATH)
except Exception as e:
  print("Deleting previous masks and creating new...")
  subprocess.call('rm -rf {}'.format(MASKS_PATH), shell=True)
  os.makedirs(MASKS_PATH)
  # remove_files(MASKS_PATH)

if LABELS_FORMAT == "labelme":
  for json_file in os.listdir(JSON_PATH):
    if not json_file.endswith(".json"):
      continue
    # Read JSON file
    file_name = os.path.join(JSON_PATH,json_file)
    with open(file_name) as f:
      data = json.load(f)
    # Read Image file and get shape
    image_name = os.path.join(RAW_IMAGES_PATH,json_file[:-5]+RAW_IMAGES_EXTENSION)
    img = cv2.imread(image_name,0)
    mask_width, mask_height = img.shape

    # Determine if there is more than one class on the image
    class_ids = np.unique([shapes['group_id'] for shapes in data['shapes']])

    mask = np.zeros((mask_width, mask_height))
    if len(data['shapes']) == 0:
      print("No shapes, printing empy mask")
      continue
    for class_id in range(len(class_ids)):
      file_bbs = []
      for shape in data["shapes"]:
        if shape['group_id'] == class_id:
          file_bbs.append(shape["points"])
      arr = np.array(file_bbs,dtype=object)
      for i in range(len(arr)):
        cv2.fillPoly(mask, np.int32([arr[i]]), color=(class_id+1))
    cv2.imwrite(os.path.join(MASKS_PATH, json_file[:-5] + "_mask"+MASKS_EXTENSION) , mask)
elif LABELS_FORMAT == "coco":
  for json_file in os.listdir(JSON_PATH):
    file_name = os.path.join(JSON_PATH,json_file)
    coco = COCO(file_name)
    for key in list(coco.imgs.keys()):
      img = coco.imgs[key]
      filename = img['file_name'][:-4]
      cat_ids = coco.getCatIds()
      anns_ids = coco.getAnnIds(imgIds=img['id'], catIds=cat_ids, iscrowd=None)
      anns_prev = coco.loadAnns(anns_ids)
      cat_orders = sorted(coco.cats,key=lambda d: coco.cats[d]['name'])
      anns = [[d for d in anns_prev if 
              d['category_id']==val] for val in cat_orders]
      anns = sum(anns,[])
      filtered_lbs = {len(coco.cats) : []}
      mask = np.zeros((img['height'],img['width']))
      
      for annot in anns:
        category_id = cat_orders.index(annot['category_id'])
        if category_id not in filtered_lbs: # Esta línea aún no se comprueba que funcione de manera general
          filtered_lbs[category_id], labels_list, poligons = {'intersection': [],'normal': []},[],[]
        for annot_idx,points in enumerate(annot['segmentation']):
          if annot_idx == 0:
            label_coords = [[a,b] for a,b in zip(points[::2],points[1::2])]
            current_polygon = Polygon(label_coords)
            filtered_lbs[category_id]['normal'].append(label_coords)
            labels_list.append(label_coords)
            poligons.append(current_polygon)
            continue
          label_coords = [[a,b] for a,b in zip(points[::2],points[1::2])]
          try:
            current_polygon = Polygon(label_coords)
          except ValueError as e:
            print("The error is", e)
            errors.append(e)
            continue
          intersections = [poligon.intersects(current_polygon) for poligon in poligons]
          intersection_idxs = [index for (index, intersect) in enumerate(intersections) if intersect == True]
          if len(intersection_idxs) == 0:
            filtered_lbs[category_id]['normal'].append(label_coords)
          else:
            polygon_compared = [(i, current_polygon.length > poligons[i].length) for i in intersection_idxs]
            actual_poly_bigger = [value[0] for value in polygon_compared if value[1] == True]
            prev__poly_bigger = [value[0] for value in polygon_compared if value[1] == False]
            [filtered_lbs[len(coco.cats)].append(labels_list[i]) for i in actual_poly_bigger]
            if len(prev__poly_bigger) > 0:
              filtered_lbs[len(coco.cats)].append(label_coords)
            if len(actual_poly_bigger) > 0 and len(prev__poly_bigger) == 0:
              filtered_lbs[category_id]['intersection'].append(label_coords)
            elif len(actual_poly_bigger) == 0 and len(prev__poly_bigger) > 0:
              [filtered_lbs[category_id]['intersection'].append(labels_list[i]) for i in 
              prev__poly_bigger if labels_list[i] not in filtered_lbs[category_id]['intersection']]
            else:
              filtered_lbs[category_id]['intersection'].append(label_coords)
              [filtered_lbs[category_id]['intersection'].append(labels_list[i]) for i in prev__poly_bigger]
            try:
              [filtered_lbs[category_id]['normal'].pop(filtered_lbs[category_id]['normal'].index(labels_list[i])) 
              for i in actual_poly_bigger]
            except: pass
            try:
              [filtered_lbs[category_id]['normal'].pop(filtered_lbs[category_id]['normal'].index(labels_list[i])) 
              for i in prev__poly_bigger]
            except: pass
          labels_list.append(label_coords)
          poligons.append(current_polygon)
        array_intersect = np.array(filtered_lbs[category_id]['intersection'],dtype=object)
        for i in range(len(array_intersect)):
          cv2.fillPoly(mask, np.int32([array_intersect[i]]), color=(category_id+1))
      try:
        array_zeros = np.array(filtered_lbs[len(coco.cats)],dtype=object)
        for i in range(len(array_zeros)):
          cv2.fillPoly(mask, np.int32([array_zeros[i]]), color=(0))
      except: 
        pass
      for cat in filtered_lbs.items():
        if isinstance(cat[1],dict):
          arr_normal = np.array(cat[1]['normal'],dtype=object)
          for i in range(len(arr_normal)):
            cv2.fillPoly(mask, np.int32([arr_normal[i]]), color=(cat[0]+1))
        else:
          continue
      cv2.imwrite(os.path.join(MASKS_PATH, filename + "_mask"+MASKS_EXTENSION) , mask)