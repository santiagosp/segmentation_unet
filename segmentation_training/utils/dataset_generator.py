from sklearn.model_selection import train_test_split
from relevant_functions import copy_files
import configparser
import os




config = configparser.RawConfigParser()
config.read('config/config.ini')

training_config = dict(config.items('U-NET'))
ROOT_PATH = training_config['root_path']
VAL_PROP = float(training_config['val_prop'])
TEST_PROP = float(training_config['test_prop'])

MASKS_PATH = os.path.join(ROOT_PATH,"data/masks")
JSON_PATH = os.path.join(ROOT_PATH,"data/json")
RAW_IMAGES_PATH = os.path.join(ROOT_PATH,"data/images")
SEGMENTATION_PATH = os.path.join(ROOT_PATH, "segmentation")
RAW_IMAGES_TRAIN_FOLDER = os.path.join(SEGMENTATION_PATH,"train")
ANNOTATIONS_TRAIN_FOLDER = os.path.join(SEGMENTATION_PATH,"trainannot")
RAW_IMAGES_VAL_FOLDER = os.path.join(SEGMENTATION_PATH,"val")
ANNOTATIONS_VAL_FOLDER = os.path.join(SEGMENTATION_PATH,"valannot")
RAW_IMAGES_TEST_FOLDER = os.path.join(SEGMENTATION_PATH,"test")
ANNOTATIONS_TEST_FOLDER = os.path.join(SEGMENTATION_PATH,"testannot")

RAW_IMAGES_EXTENSION = training_config['images_extension']
MASKS_EXTENSION = ".png"
MASKS_IDENTIFIER = "_mask"

# Get name of files from paths
x = os.listdir(RAW_IMAGES_PATH)
y = os.listdir(MASKS_PATH)
# Sort by name in descending order
# Raw images must have a .jpg extension
# Masks must have a .png extension and same name as raw image plus _mask 
x.sort(reverse=False, key=lambda x:x.split(RAW_IMAGES_EXTENSION)[0])
y.sort(reverse=False, key=lambda x:x.split(MASKS_IDENTIFIER+MASKS_EXTENSION)[0])

# Check if names of raw image and label coincide after sorting
assert (x[0].split(".")[0] + MASKS_IDENTIFIER) == y[0].split(".")[0]

# Split dataset in 
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=TEST_PROP)
x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, test_size=VAL_PROP)

x_train_dir = os.path.join(ROOT_PATH, SEGMENTATION_PATH, RAW_IMAGES_TRAIN_FOLDER)
y_train_dir = os.path.join(ROOT_PATH, SEGMENTATION_PATH, ANNOTATIONS_TRAIN_FOLDER)

x_val_dir = os.path.join(ROOT_PATH, SEGMENTATION_PATH, RAW_IMAGES_VAL_FOLDER)
y_val_dir = os.path.join(ROOT_PATH, SEGMENTATION_PATH, ANNOTATIONS_VAL_FOLDER)

x_test_dir = os.path.join(ROOT_PATH, SEGMENTATION_PATH, RAW_IMAGES_TEST_FOLDER)
y_test_dir = os.path.join(ROOT_PATH, SEGMENTATION_PATH, ANNOTATIONS_TEST_FOLDER)

copy_files(x_train, ROOT_PATH, RAW_IMAGES_PATH, RAW_IMAGES_TRAIN_FOLDER)
copy_files(y_train, ROOT_PATH,  MASKS_PATH, ANNOTATIONS_TRAIN_FOLDER)
copy_files(x_val, ROOT_PATH,  RAW_IMAGES_PATH, RAW_IMAGES_VAL_FOLDER)
copy_files(y_val, ROOT_PATH,  MASKS_PATH, ANNOTATIONS_VAL_FOLDER)
copy_files(x_test, ROOT_PATH,  RAW_IMAGES_PATH, RAW_IMAGES_TEST_FOLDER)
copy_files(y_test, ROOT_PATH,  MASKS_PATH, ANNOTATIONS_TEST_FOLDER)