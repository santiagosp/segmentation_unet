#!/bin/bash

# Select the correct cuda version
sudo rm /usr/local/cuda
sudo ln -s /usr/local/cuda-10.0 /usr/local/cuda

# Enable GPU
nvidia-smi

# pip installation
# bash utils/pip_install.sh


# Create setup files
# python3 -m pip uninstall -y h5py opencv-python
python3 -m pip install -r config/requirements.txt
python3 utils/training_setup.py